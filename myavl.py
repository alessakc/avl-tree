#!/usr/bin/python2.7
import sys

class Node():
	def __init__(self, key, height):
		self.key = key
		self.height = height
		self.leftChild = None
		self.rightChild = None

class AVL():
	def __init__(self):
		self.node = None

	# calcula a altura com a raiz comecando por 1
	def heightTree(self):
		if self.node == None:
			return 0
		else:
			nLeft = 1 + self.node.leftChild.heightTree()
			nRight = 1 + self.node.rightChild.heightTree()
		if nLeft > nRight:
			return nLeft
		else:
			return nRight

	# calcula o fator de balanceamento
	def balancingFactor(self):
		lHeight = self.node.leftChild.heightTree()
		rHeight = self.node.rightChild.heightTree()

		return lHeight - rHeight


	# percorre a arvore e ajusta a altura conforme o a altura do pai
	def adjustHeight(self, height):
		if self.node == None:
			return	
		else:
			# nodo recebe altura do pai + 1
			self.node.height = height + 1
			# ajusta a altura da subarvore da esquerda
			self.node.leftChild.adjustHeight(self.node.height)
			# ajusta a altura da subarvore da direita
			self.node.rightChild.adjustHeight(self.node.height)

	# rotacao para a direita
	def rightRotate(self):
		# guarda o Y
		y = self.node.leftChild
		# guarda o X
		x = self.node 
		# guarda o filho da direita de y
		yRightChild = y.node.rightChild.node 
		# o x troca de lugar com o y
		self.node = y.node
		# o filho da direita de y recebe x
		y.node.rightChild.node = x 
		# o filho da esquerda de x recebe o filho da direita de y
		x.leftChild.node = yRightChild 
		# decrementa 2 da altura do y, 1 pq ele sobe e 1 pra compensar a funcao
		self.node.height -= 2
		# chama a funcao para ajustar as alturas
		self.adjustHeight(self.node.height)

	# rotacao para a esquerda
	def leftRotate(self):
		# guarda o Y
		y = self.node.rightChild
		# guarda o X
		x = self.node 
		# guarda o filho da esquerda de y
		yLeftChild = y.node.leftChild.node 
		self.node = y.node
		# o x troca de lugar com o y
		y.node.leftChild.node = x 
		# o filho da direita de x recebe o filho da esquerda de y
		x.rightChild.node = yLeftChild 
		# decrementa 2 da altura do y, 1 pq ele sobe e 1 pra compensar a funcao
		self.node.height -= 2
		# chama a funcao para ajustar as alturas
		self.adjustHeight(self.node.height)

	# faz a insercao e confere se esta balanceado
	def insert(self, key, height):
		# se a arvore estiver vazia
		if self.node == None:
			self.node = Node(key, 0);
			self.node.rightChild = AVL()
			self.node.leftChild = AVL()
			self.node.height = height + 1
		# insere na direita
		elif key > self.node.key:
			self.node.rightChild.insert(key, self.node.height)
		# insere na esquerda
		else:
			self.node.leftChild.insert(key, self.node.height)

		# b recebe o fator de balanceamento
		b = self.balancingFactor()
		# faz as rotacoes de balanceamento
		if b > 1:
			# esquerda direita
			if key > self.node.leftChild.node.key:
				self.node.leftChild.leftRotate()
				self.rightRotate()
            # esquerda esquerda
			else:
				self.rightRotate()
		if b < -1:
			# direita direita
			if key > self.node.rightChild.node.key:
				self.leftRotate()
			# direita esquerda
			else:
				self.node.rightChild.rightRotate()
				self.leftRotate()
		

	def searchPredecessor(self):
		# n recebe o filho da esquerda
		n = self.node.leftChild
		while n.node.rightChild.node != None:
			n = n.node.rightChild
		return n

	def delete(self, key):
		# procura o nodo a ser deletado
		if self.node == None:
			return 
		# procura na direita
		elif key > self.node.key:
			self.node.rightChild.delete(key)
		# procura na esquerda
		elif key < self.node.key:
			self.node.leftChild.delete(key)
		# se encontrou o nodo
		elif key == self.node.key:
			# se for o folha
			if self.node.leftChild.node == None and self.node.rightChild.node == None:
				self.node.key = None
				self.node.height = 0
				self.node = None
				return
			# se tiver apenas um filho na esquerda
			elif self.node.rightChild.node == None:
				self.node = self.node.leftChild.node
				self.node.height = 0
			# se tiver apenas um filho na direita
			elif self.node.leftChild.node == None:
				self.node = self.node.rightChild.node
				self.node.height = 0
			# se tiver os dois filhos
			else:
				# procura o antecessor
				p = self.searchPredecessor()
				# substitui pelo antecessor
				self.node.key = p.node.key
				self.node.leftChild.delete(p.node.key)

		b = self.balancingFactor()
		if b > 1:
			# diferenca das alturas dos filhos do filho da esquerda
			b2 = self.node.leftChild.balancingFactor
			# esquerda direita
			if b2 < 0:
				self.node.leftChild.leftRotate()
				self.rightRotate()
            # esquerda esquerda
			else:
				self.rightRotate()
		if b < -1:
			# diferenca das alturas dos filhos do filho da direita
			b2 = self.node.rightChild.balancingFactor
			# direita direita
			if key > self.node.rightChild.node.key:
				self.leftRotate()
			# direita esquerda
			else:
				self.node.rightChild.rightRotate()
				self.leftRotate()

	# imprime a arvore em ordem
	def printTree(self):
		if self.node == None:
			return
		self.node.leftChild.printTree()
		print str(self.node.key) + "," + str(self.node.height)
		self.node.rightChild.printTree()


def main():
	tree = AVL()
	# pra cada linha da entrada
	for line in sys.stdin:
		# separa os argumentos
		k = str(line).split()
		# se for insercao
		if k[0] == 'i':
			tree.insert(int(k[1]), -1)
		# se for remocao
		elif k[0] == 'r':
			tree.delete(int(k[1]))

	tree.printTree()

if __name__ == "__main__":
	main()

